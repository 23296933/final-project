//Page Navigation Function
// Variable
var fixedNav = document.getElementById("nav");
window.onscroll = function() {
	// When page is scrolled past 700 from the top, it fixed to the top of the browser
	if(window.pageYOffset > 700) {
 		fixedNav.style.position = "fixed";
 		fixedNav.style.top = 0;
 		fixedNav.style.marginTop = "-30px"
 	} else {
 		// When the page is under 700, it goes back to normal
 		fixedNav.style.position = "absolute";
 		fixedNav.style.top = "600px";
 	}
}
//Search/Filter Function
function searchFunction() {
  // Declare variables 
 	var input, filter, tr, td, i, data, k;
  	input = document.getElementById("userInput");
  	filter = input.value.toUpperCase();
  	tr = document.getElementsByClassName("list-item");

  	//Loops through the table rows
  	for (i = 0; i < tr.length; i++) {
  		tr[i].style.display = "none";
    	td = tr[i].getElementsByTagName("TD");
    	for (var k = 0; k < td.length; k++) {
    		//Loops through all the data. Not just one column of data
    		data = tr[i].getElementsByTagName("td")[k];
    		if (data) {
    			//When the input does not match, the row will disappear
    			if (data.innerText.toUpperCase().indexOf(filter) > -1) {
    				tr[i].style.display = "";
    				break;
    			}
    		}
    	}
  	}
}